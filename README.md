# Introduction
The objective of this project is to demonstrate how to create a MySQL 8.0 InnoDB cluster on Kubenetes. While several "operators" exist for doing that, the idea here is to start from the InnoDB cluster requirements and manually build the necessary Kubernetes objects to support such a deployment.

We will follow and adapt the installtion instructions from the MySQL official documentation:
https://dev.mysql.com/doc/refman/8.0/en/mysql-innodb-cluster-production-deployment.html

After completing this guide we will have deployed the following architecture:
![cluster.png](diagram/cluster.png)

# Pre-requisites
We assume that you have a running Kubernetes cluster with at least 3 nodes, and kubectl properly configured to access it. 

# Components
MySQL InnoDB clutsers are based on 3 main components:
- MySQL Server: this is the usual MySQL Server. 
- MySQL Router: the endpoint that applications will connect to in order to access the MySQL databases. 
- MySQL Shell: the simplified administration interface that can run on the MySQL servers or remotely. We will run the MySQL Shell on a dedicated container.

# Step-by-step installation
### MySQL Nodes
We will start by creating 3 MySQL pods, each with a dedicated volume claim, and a headless service so we can access them easily. This initial configuration will be created using the StatefulSet from `mysql-servers.yml`:

```
$ kubectl create -f  k8s/mysql-servers.yml
```

Let's check what has been deployed:
```
$ kubectl get all
NAME          READY   STATUS    RESTARTS   AGE
pod/mysql-0   1/1     Running   0          4m14s
pod/mysql-1   1/1     Running   0          4m9s
pod/mysql-2   1/1     Running   0          3m1s

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP    7d19h
service/mysql        ClusterIP   None         <none>        3306/TCP   4m16s

NAME                     READY   AGE
statefulset.apps/mysql   3/3     4m16s

$ kubectl get pvc
NAME           STATUS   VOLUME                                                                                         CAPACITY   ACCESS MODES   STORAGECLASS   AGE
data-mysql-0   Bound    ocid1.volume.oc1.eu-frankfurt-1.abtheljsaqqpqgtdumnbzcquuvkgpb6kreodpwsky6khqeuimsylrp4grouq   50Gi       RWO            oci            18m
data-mysql-1   Bound    ocid1.volume.oc1.eu-frankfurt-1.abtheljtmuokcyom4ccqie435kxlx4igciyyxvbn2e5jezx2cfye74u3srca   50Gi       RWO            oci            12m
data-mysql-2   Bound    ocid1.volume.oc1.eu-frankfurt-1.abtheljrx2rbq62oqzxrephatcwdb5ogux2ozpfhjxd4wgcspgoo2e25ujja   50Gi       RWO            oci            2m54s
```

Here are a few notes regarding this configuration:
- we used an init container to get the server id and update the MySQL server configuration
- we did not use affinity or anti-affinity rules to keep the configuration light.

### MySQL Shell

We will deploy a dedicated container for the MySQL Shell. This will allow us to validate that mysql containers can talk to each other. 

As there is no official Docker image available for MySQL Shell yet, we can create one based on the buster-slim image (same base image on which MySQL is built). We provided a sample Dockerfile in the `mysqlsh` folder. 

To build the image enter the following command (here swissledger/mysqlsh is the name of the community image that we created): 
```
$ docker build -t swissledger/mysqlsh ./mysqlsh
```

After pushing the docker image on your repository, you can deploy it on kubernetes using this command:
```
$ kubectl create -f k8s/mysql-shell.yml
```

### Validate mysql server instances meet all requirements
We use MySQL shell to do that:
```
$ kubectl exec -it mysql-shell /bin/bash
root@mysql-shell:/# mysqlsh
MySQL  JS > dba.checkInstanceConfiguration('root:secret@mysql-0.mysql:3306')
...
The instance 'mysql-0:3306' is valid to be used in an InnoDB cluster.
...
MySQL  JS > dba.checkInstanceConfiguration('root:secret@mysql-1.mysql:3306')
...
The instance 'mysql-1:3306' is valid to be used in an InnoDB cluster.
...
MySQL  JS > dba.checkInstanceConfiguration('root:secret@mysql-2.mysql:3306')
...
The instance 'mysql-2:3306' is valid to be used in an InnoDB cluster.
...
```

### Create cluster
We will also create the cluster from the MySQL shell (ipWhitelist should be replaced with your Kubernetes network configuration):
```
$ kubectl exec -it mysql-shell /bin/bash
root@mysql-shell:/# mysqlsh --uri root:secret@mysql-0.mysql:3306
MySQL  mysql-0.mysql:3306 ssl  JS > var cluster = dba.createCluster('prodCluster', {ipWhitelist: "10.244.0.0/16"})
MySQL  mysql-0.mysql:3306 ssl  JS > cluster.addInstance('root:secret@mysql-1.mysql:3306', {recoveryMethod: 'Clone', ipWhitelist: "10.244.0.0/16"})
MySQL  mysql-0.mysql:3306 ssl  JS > cluster.addInstance('root:secret@mysql-2.mysql:3306', {recoveryMethod: 'Clone', ipWhitelist: "10.244.0.0/16"})
MySQL  mysql-0.mysql:3306 ssl  JS > cluster.status()
```
Note that if an instance shows an invalid status, you can rejoin it using a command like that:
```
cluster.rejoinInstance('root:secret@mysql-1.mysql:3306')
```
You can also reconnect to a cluster using its name:
```
MySQL  mysql-0.mysql:3306 ssl  JS > var cluster = dba.getCluster('prodCluster')
MySQL  mysql-0.mysql:3306 ssl  JS > cluster.status()
```

### MySQL Router
We will use the MySQL Router image provided by MySQL: 
```
$ kubectl create -f k8s/mysql-router.yml
```
Note that port mapping in the service definition: MySQL port is exposed by the MySQL router on port 6446 and mapped to 3306, while MySQLX is exposed on port 64460 and mapped to 33060. Now let's check that we connect to the MySQL cluster through the router:
```
$ kubectl create -f k8s/mysql-client.yml
$ kubectl exec -it mysql-client /bin/bash
# mysql -u root -p --password=secret -h mysql-router
```
Let's play with a Python application and create some JSON documents:
```
$ kubectl create -f k8s/application.yml 
$ kubectl exec -it application /bin/bash
# pip install mysql-connector-python
# python
>>> import mysqlx
>>> session = mysqlx.get_session("root:secret@mysql-router")
>>> schema = session.create_schema("test")
>>> collection = schema.create_collection("my_collection")
>>> collection.add({"name": "Sakila", "age": 15}, {"name": "Jack", "age": 32}, {"name": "Clare", "age": 37}).execute()
>>> result = collection.find("age > 18").execute()
>>> print(result.fetch_all())
[{'_id': '00005e6689110000000000000002', 'age': 32, 'name': 'Jack'}, {'_id': '00005e6689110000000000000003', 'age': 37, 'name': 'Clare'}]
```

# Testing the Cluster Resiliency
Let's kill the mysql-2 pod and see what happens:
```
$ kubectl delete pod/mysql-2
```
You should see that the intance is reprovisionned automatically (remember we created a StatefulSet). 
If we connect to the MySQL Shell, we can check the status of the cluster:
```
$ kubectl exec -it mysql-shell /bin/bash
# mysqlsh --uri root:secret@mysql-0.mysql:3306
MySQL  mysql-0.mysql:3306 ssl  JS > var cluster = dba.getCluster('prodCluster')
MySQL  mysql-0.mysql:3306 ssl  JS > cluster.status()
{
    "clusterName": "prodCluster", 
    "defaultReplicaSet": {
        "name": "default", 
        "primary": "mysql-0.mysql:3306", 
        "ssl": "REQUIRED", 
        "status": "OK", 
        "statusText": "Cluster is ONLINE and can tolerate up to ONE failure.", 
        "topology": {
            "mysql-0.mysql:3306": {
                "address": "mysql-0.mysql:3306", 
                "mode": "R/W", 
                "readReplicas": {}, 
                "replicationLag": null, 
                "role": "HA", 
                "status": "ONLINE", 
                "version": "8.0.19"
            }, 
            "mysql-1.mysql:3306": {
                "address": "mysql-1.mysql:3306", 
                "mode": "R/O", 
                "readReplicas": {}, 
                "replicationLag": null, 
                "role": "HA", 
                "status": "ONLINE", 
                "version": "8.0.19"
            }, 
            "mysql-2.mysql:3306": {
                "address": "mysql-2.mysql:3306", 
                "mode": "R/O", 
                "readReplicas": {}, 
                "replicationLag": null, 
                "role": "HA", 
                "status": "ONLINE", 
                "version": "8.0.19"
            }
        }, 
        "topologyMode": "Single-Primary"
    }, 
    "groupInformationSourceMember": "mysql-0.mysql:3306"
}
```

If we kill the mysql-1 pod, we will see the same behaviour.

Now let's see what happens if we kill the mysql-0 pod:
```
$ kubectl exec -it mysql-shell /bin/bash
# mysqlsh --uri root:secret@mysql-1.mysql:3306
MySQL  mysql-0.mysql:3306 ssl  JS > var cluster = dba.getCluster('prodCluster')
MySQL  mysql-0.mysql:3306 ssl  JS > cluster.status()
{
    "clusterName": "prodCluster", 
    "defaultReplicaSet": {
        "name": "default", 
        "primary": "mysql-1.mysql:3306", 
        "ssl": "REQUIRED", 
        "status": "OK", 
        "statusText": "Cluster is ONLINE and can tolerate up to ONE failure.", 
        "topology": {
            "mysql-0.mysql:3306": {
                "address": "mysql-0.mysql:3306", 
                "mode": "R/O", 
                "readReplicas": {}, 
                "replicationLag": null, 
                "role": "HA", 
                "status": "ONLINE", 
                "version": "8.0.19"
            }, 
            "mysql-1.mysql:3306": {
                "address": "mysql-1.mysql:3306", 
                "mode": "R/W", 
                "readReplicas": {}, 
                "replicationLag": null, 
                "role": "HA", 
                "status": "ONLINE", 
                "version": "8.0.19"
            }, 
            "mysql-2.mysql:3306": {
                "address": "mysql-2.mysql:3306", 
                "mode": "R/O", 
                "readReplicas": {}, 
                "replicationLag": null, 
                "role": "HA", 
                "status": "ONLINE", 
                "version": "8.0.19"
            }
        }, 
        "topologyMode": "Single-Primary"
    }, 
    "groupInformationSourceMember": "mysql-1.mysql:3306"
}
```
We see that the mysql-1 node took over the master role and changed to R/W mode. mysql-0 joined back the cluster in R/O mode.

Now let's go back to our python container and check that we can still connect to the cluster with the same connection method:
```
$ kubectl exec -it application /bin/bash
# pip install mysql-connector-python
# python
>>> import mysqlx
>>> session = mysqlx.get_session("root:secret@mysql-router")
>>> schema = session.get_schema("test")
>>> collection = schema.get_collection("my_collection")
>>> result = collection.find().execute()
>>> print(result.fetch_all())
[{'_id': '00005e6689110000000000000001', 'age': 15, 'name': 'Sakila'}, {'_id': '00005e6689110000000000000002', 'age': 32, 'name': 'Jack'}, {'_id': '00005e6689110000000000000003', 'age': 37, 'name': 'Clare'}]
```

In case you completely stop the cluster (e.g. stop all the nodes from the Kubernetes cluster), you can reboot the cluster using this command:
```
mysqlsh --uri root:secret@mysql-0.mysql:3306
MySQL  mysql-0.mysql:3306 ssl  JS > var cluster = dba.rebootClusterFromCompleteOutage();
```

# Appendix A: using OpenShift
In this section we will check for any specificities that might be introduced by using OpenShift instead of a vanilla installation of K8s.

We will make our tests with OpenShift 4.3 installed on MacOS using the Code Ready Containers: https://cloud.redhat.com/openshift/install/crc/installer-provisioned

```
crc start
INFO Starting OpenShift cluster ... [waiting 3m]  
INFO                                              
INFO To access the cluster, first set up your environment by following 'crc oc-env' instructions 
INFO Then you can access it by running 'oc login -u developer -p developer https://api.crc.testing:6443' 
INFO To login as an admin, run 'oc login -u kubeadmin -p kKdPx-pjmWe-b3kuu-jeZm3 https://api.crc.testing:6443' 
INFO                                              
INFO You can now run 'crc console' and use these credentials to access the OpenShift web console 
Started the OpenShift cluster

crc status
CRC VM:          Running
OpenShift:       Running (v4.3.8)
Disk Usage:      12.37GB of 32.72GB (Inside the CRC VM)
Cache Usage:     11.95GB
Cache Directory: /Users/goutaudi/.crc/cache
```

We can now login to OpenShift. Pay attention that this will merge your existing kubectl configuration file with the OpenShift one. In my case I prefer to have separate config files for each cluster and update the KUBECONFIG environment to switch from one cluster to the other, so I did a backup of my `$HOME/.kube/config` file and removed it before running the below commands:  

```
$ eval $(crc oc-env)
$ oc login -u kubeadmin -p kKdPx-pjmWe-b3kuu-jeZm3 https://api.crc.testing:6443
$ oc version
Client Version: 4.3.10-202003280552-6a90d0a
Server Version: 4.3.8
Kubernetes Version: v1.16.2
```

The `oc` command behaves exactly like `kubectl` (with additional features). To deploy the InnoDB cluster, we will follow all the instructions swapping these 2 commands:
```
$ oc create -f  k8s/mysql-servers.yml
$ oc create -f k8s/mysql-shell.yml
$ oc exec -it mysql-shell /bin/bash
root@mysql-shell:/# mysqlsh --uri root:secret@mysql-0.mysql:3306
MySQL  mysql-0.mysql:3306 ssl  JS > var cluster = dba.createCluster('prodCluster', {ipWhitelist: "10.128.0.0/16"})
MySQL  mysql-0.mysql:3306 ssl  JS > cluster.addInstance('root:secret@mysql-1.mysql:3306', {recoveryMethod: 'Clone', ipWhitelist: "10.128.0.0/16"})
MySQL  mysql-0.mysql:3306 ssl  JS > cluster.addInstance('root:secret@mysql-2.mysql:3306', {recoveryMethod: 'Clone', ipWhitelist: "10.128.0.0/16"})
MySQL  mysql-0.mysql:3306 ssl  JS > cluster.status()
MySQL  mysql-0.mysql:3306 ssl  JS > \q
```

Now we will deploy a python application and connect to the cluster through a MySQL router:
```
$ oc create -f k8s/mysql-router.yml
$ oc create -f k8s/mysql-client.yml
$ oc create -f k8s/application.yml
$ oc exec -it application /bin/bash
# pip install mysql-connector-python
# python
>>> import mysqlx
>>> session = mysqlx.get_session("root:secret@mysql-router")
>>> schema = session.create_schema("test")
>>> collection = schema.create_collection("my_collection")
>>> collection.add({"name": "Sakila", "age": 15}, {"name": "Jack", "age": 32}, {"name": "Clare", "age": 37}).execute()
>>> result = collection.find("age > 18").execute()
>>> print(result.fetch_all())
[{'_id': '00005e6689110000000000000002', 'age': 32, 'name': 'Jack'}, {'_id': '00005e6689110000000000000003', 'age': 37, 'name': 'Clare'}]
```

# Appendix B: Setting up replication
InnoDB clusters work well when the servers are close to each other. When synchronisation with a remote environment is required, or when some lag is acceptable (to run anlytics for example), it can be interesting to use an asynchronous replication mechanism between the InnoDB cluster and another MySQL server. 

In this section we will configure a transaction-based replication following these instructions:
https://dev.mysql.com/doc/refman/8.0/en/replication-gtids.html

We start by creating the new server:
```
$ oc create -f k8s/mysql-replica.yml 
```

Then we configure the replication:
```
$ oc exec -it mysql-replica-0 /bin/bash
# mysql -u root -p --password=secret 
mysql> CHANGE MASTER TO MASTER_HOST = 'mysql-router', MASTER_PORT = 3306, MASTER_USER = 'root', MASTER_PASSWORD = 'secret', MASTER_AUTO_POSITION = 1;
mysql> START SLAVE;
```

Finally we check that the replication was effective:
```
$ oc exec -it application /bin/bash
python
>>> import mysqlx
>>> session = mysqlx.get_session("root:secret@mysql-replica")
>>> schema = session.get_schema("test")
>>> collection = schema.get_collection("my_collection")
>>> result = collection.find().execute()
>>> print(result.fetch_all())
[{'_id': '00005e9d869d0000000000000001', 'age': 15, 'name': 'Sakila'}, {'_id': '00005e9d869d0000000000000002', 'age': 32, 'name': 'Jack'}, {'_id': '00005e9d869d0000000000000003', 'age': 37, 'name': 'Clare'}]
```




